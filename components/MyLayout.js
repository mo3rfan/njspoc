import Header from './Header'
import fetch from 'node-fetch'
import ApolloProvider from 'react-apollo'
import ApolloClient from 'apollo-boost'

const layoutStyle = {
  margin: 20,
  padding: 20,
  border: '1px solid #DDD'
}

const client = new ApolloClient({uri: 'http://localhost:4000'})

const Layout = (props) => (
  <ApolloProvider client={client}>
    <div style={layoutStyle}>
      <Header />
      {props.children}
    </div>
  </ApolloProvider>
)

export default Layout
