const express = require('express')
const next = require('next')
var cors = require('cors')

const dev = process.env.NODE_ENV !== 'production'
const app = next({dev})
const handle = app.getRequestHandler()

const bookshelf = require('./bookshelf')
const Joi = require('joi')

app.prepare()
.then(() => {
  // Apollo GraphQL
  const {ApolloServer, gql} = require('apollo-server')
  const typeDefs = gql`
    type Username {
      username: String!
    }
    type User {
      username: Username
      password: String
    }
    type File {
      filename: String!
      mimetype: String!
      encoding: String!
    }

    type Query {
      validatePassword(username: String, password: String): Status
      getUsers: [Username]
      uploads: [File]
    }
    type Status {
      statusCode: Int
      statusMessage: String
    }
    
    type Mutation {
      register(username: String, password: String): [Status]
      updatePassword(username: String, old_password: String, new_password: String): [Status]
      deleteUser(username: String, password: String): [Status]
      uploadDoc(file: Upload!): [Status]
    }
  `
  var User = bookshelf.Model.extend({
    tableName: 'users',

    verifyPassword: function (password) {
      return this.get('password') === password
    }
  })
 
  const resolvers = {
    Query: {
      validatePassword(parent, args) {
        const schema = Joi.object().keys({
          username: Joi.string().alphanum().required(),
          password: Joi.string().alphanum().required()
        })
        return Joi.validate(args, schema).then(value => {
          return User.fetch({username: value.username}).then(model => {
            if (model.get('password') === args.password) {
              return {statusMessage: 'Password match!'}
            }
            return {statusMessage: 'Password mismatch!'}
          })
        }).catch(err => ({statusMessage: err.label}))
      },
      getUsers() {
        return User.fetchAll({columns: ['username']}).then(collection => collection.serialize())
      },
      uploads() {

      }
    },
    Mutation: {
      async uploadDoc(parent, {file}) {
        const {stream, filename, mimetype, encoding} = await file
        // stream the filecontents into MongoDB. (Use BSON?)
        return {filename, mimetype, encoding}
      },
      async register(parent, args) {
        var user = {
          username: args.username,
          password: args.password
        }
        const schema = Joi.object().keys({
          username: Joi.string().alphanum().required(),
          password: Joi.string().alphanum().required()
        })
        return Joi.validate(args, schema).then(() => {
          return User.forge(user).fetch().then(model => {
            if (model !== null) {
              return [{statusMessage: 'Already exists!'}]
            }
            return User.forge(user).save().then(() => ([{statusMessage: 'User added'}]))
          })
        }).catch(err => err.details.map(v => ({statusMessage: v.message})))
      },
      async updatePassword(parent, args) {
        const schema = Joi.object().keys({
          username: Joi.string().alphanum().required(),
          password: Joi.string().alphanum().required()
        })
        return Joi.validate(args, schema).then(() => {
          return User.forge({username: args.username}).fetch().then(model => {
            if (model.get('password') === args.old_password) {
              return model.set('password', args.new_password).save().then(() => ([{statusMessage: 'Password changed!'}]))
            }
            return [{statusMessage: 'Password mismatch!'}]
          })
        }).catch(err => err.details.map(v => ({statusMessage: v.message})))
      },
      async deleteUser(parent, args) {
        const schema = Joi.object().keys({
          username: Joi.string().alphanum().required(),
          password: Joi.string().alphanum().required()
        })
        return Joi.validate(args, schema).then(() => {
          return User.forge({username: args.username}).fetch().then(model => {
            if (model === null) {
              return [{statusMessage: 'User does not exist!'}]
            }
            if (model.get('password') === args.password) {
              return model.destroy().then(() => ([{statusMessage: 'User deleted!'}]))
            }
            return [{statusMessage: 'Password mismatch!'}]
          })
        }).catch(err => err.details.map(v => ({statusMessage: v.message})))
      }
    }
  }

  const apolloServer = new ApolloServer({typeDefs, resolvers})
  apolloServer.listen(4000, '0.0.0.0').then(({url}) => {
    console.log(`Apollo 🚀 Server ready at ${url}`)
  })
  // Normal APIs
  const server = express()
  console.log('Express server loading...')
  server.get('/p/:id', cors(), (req, res) => {
    const actualPage = '/post'
    const queryParams = {id: req.params.id}
    app.render(req, res, actualPage, queryParams)
  })

  server.get('*', cors(), (req, res) => {
    return handle(req, res)
  })

  server.listen(3000, '0.0.0.0', err => {
    if (err) {
      throw err
    }
    console.log('> Ready on http://localhost:3000')
  })
})
.catch(err => {
  console.error(err.stack)
  process.exit(1)
})
