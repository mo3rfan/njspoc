var knex = require('knex')({
  client: 'pg',
  connection: process.env.PG_CONNECTION_STRING,
  searchPath: ['knex', 'public']
})

knex.schema.createTable('users', function (table) {
  table.increments()
  table.string('username')
  table.string('password')
}).then(() => console.log('Creating table for users..')).catch(() => console.log('Table users already exists'))
module.exports = require('bookshelf')(knex)
