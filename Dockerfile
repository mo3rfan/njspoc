FROM node:8
RUN mkdir -p /root/app
ADD package.json /root/app
WORKDIR /root/app/
RUN npm install
ADD pages /root/app/pages
ADD components /root/app/components
ADD server.js /root/app
ADD bookshelf.js /root/app
CMD npm run build && npm run dev
